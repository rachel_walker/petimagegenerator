<?php declare(strict_types=1);

/**
 * @category     Model
 * @package      petImageGenerator
 * @author       Rachel Walker (rachel.kai@gmail.com)
 */

namespace App\Models;

use SimpleXMLElement;
use Exception;

class BodyPartsConfig
{
	//These should correspond to the names of the body part nodes used in the body parts config file.
	const BODY_PART_HEAD = "head";
	const BODY_PART_EYE = "eye";
	const BODY_PART_NECK = "neck";
	const BODY_PART_BODY = "body";
	const BODY_PART_TAIL = "tail";
	const BODY_PART_RIGHT_FRONT_LEG = "rightFrontLeg";
	const BODY_PART_LEFT_FRONT_LEG = "leftFrontLeg";
	const BODY_PART_RIGHT_HIND_LEG = "rightHindLeg";
	const BODY_PART_LEFT_HIND_LEG = "leftHindLeg";
	
	const BODY_PART_TYPES = [
		self::BODY_PART_HEAD,
		self::BODY_PART_EYE,
		self::BODY_PART_NECK,
		self::BODY_PART_BODY,
		self::BODY_PART_TAIL,
		self::BODY_PART_RIGHT_FRONT_LEG,
		self::BODY_PART_LEFT_FRONT_LEG,
		self::BODY_PART_RIGHT_HIND_LEG,
		self::BODY_PART_LEFT_HIND_LEG,
	];
	
	//These should correspond to the types of the anchor nodes used in the body parts config file.
	const ANCHOR_TYPE_EYE_HEAD = "eyeHead";
	const ANCHOR_TYPE_HEAD_EYE = "headEye";
	const ANCHOR_TYPE_NECK_HEAD = "neckHead";
	const ANCHOR_TYPE_HEAD_NECK = "headNeck";
	const ANCHOR_TYPE_BODY_NECK = "bodyNeck";
	const ANCHOR_TYPE_NECK_BODY = "neckBody";
	const ANCHOR_TYPE_BODY_TAIL = "bodyTail";
	const ANCHOR_TYPE_TAIL_BODY = "tailBody";
	
	const ANCHOR_TYPE_RIGHT_FRONT_LEG_BODY = "rightFrontLegBody";
	const ANCHOR_TYPE_LEFT_FRONT_LEG_BODY = "leftFrontLegBody";
	const ANCHOR_TYPE_RIGHT_HIND_LEG_BODY = "rightHindLegBody";
	const ANCHOR_TYPE_LEFT_HIND_LEG_BODY = "leftHindLegBody";
	
	private $bodyPartsConfigXml;
	
	public function __construct(string $bodyPartConfigFile)
	{
		if (!file_exists($bodyPartConfigFile)) {
			throw new Exception("The body part config file does not exist.");
		}
		
		$this->bodyPartsConfigXml = simplexml_load_file($bodyPartConfigFile);
	}
	
	private function isValidBodyPart(string $bodyPart) : bool
	{
		return in_array($bodyPart, self::BODY_PART_TYPES);
	}
	
	private function numberOfImagesPerBodyPart(string $bodyPart) : int
	{
		return count($this->bodyPartsConfigXml->$bodyPart->image);
	}
	
	/**
	 *  Makes sure the image number given for each body part corresponds to an actual
	 *  node in the xml file that contains all the information on images for each body part.
	 */
	private function normalizeImageNumber(string $bodyPart, int $bodyPartNumber) : int
	{
		$maxNumber = $this->numberOfImagesPerBodyPart($bodyPart);
		$bodyPartNumber = $bodyPartNumber < 0 ? 0 : $bodyPartNumber;
		$bodyPartNumber = $bodyPartNumber >= $maxNumber? 0 : $bodyPartNumber;
		return $bodyPartNumber;
	}
	
	private function getAnchors(string $bodyPart, int $bodyPartNumber) : SimpleXMLElement
	{
		return $this->bodyPartsConfigXml->$bodyPart->image[$bodyPartNumber];
	}
	
	/**
	 *  Body parts have anchor points to attach to other body parts.  
	 *  Different body part images have different anchor points locations.
	 *  This gets the anchor point information of a specific anchor type for a specific body part.
	 */
	private function getAnchor(string $bodyPart, int $bodyPartNumber, string $type) : SimpleXMLElement
	{
		foreach ($this->getAnchors($bodyPart, $bodyPartNumber) as $anchor)
		{
			if ($anchor["type"] == $type)
			{
				return $anchor;
			}
		}

        throw new Exception("Anchor type {$type} not found for {$bodyPart} #{$bodyPartNumber}");
	}
	
	public function getAnchorOffset(string $bodyPart, int $bodyPartNumber, $anchorPointName) : array
	{
		if (!$this->isValidBodyPart($bodyPart)) {
			throw new Exception("Invalid body part");
		}
		
		$bodyPartNumber = $this->normalizeImageNumber($bodyPart, $bodyPartNumber);
		
		$tempAnchor = $this->getAnchor($bodyPart, $bodyPartNumber, $anchorPointName);

		return [(int) $tempAnchor->xOffset, (int) $tempAnchor->yOffset];
	}	
	
	/**
	 * $index is either "white" or "black" as corresponding to the attributes on the image nodes in the body parts config xml.
	 */
	public function getImage(string $bodyPart, int $bodyPartNumber, string $index = "white") : string
	{		
		if (!$this->isValidBodyPart($bodyPart)) {
			throw new Exception("Invalid body part");
		}
		
		if (!in_array($index, ["black", "white"])) {
			throw new Exception("Image can only be black or white");
		}
		
		$bodyPartNumber = $this->normalizeImageNumber($bodyPart, $bodyPartNumber);
	
		return (string) $this->bodyPartsConfigXml->$bodyPart->image[$bodyPartNumber][$index];
	}
}