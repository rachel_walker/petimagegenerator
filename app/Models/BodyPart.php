<?php declare(strict_types=1);

/**
 * @category     Model
 * @package      petImageGenerator
 * @author       Rachel Walker (rachel.kai@gmail.com)
 */

namespace App\Models;

class BodyPart
{
	private $bodyPart;
	
	private $whiteImage;
	private $blackImage;
	
	private $children;
	
	//Indicates the offset of this body part from the parent anchor.
	private $xOffset;
	private $yOffset;
	
	//Indicates the relative location of the parent anchor that this part is attaching to.
	private $parentAnchorX;
	private $parentAnchorY;
	
	private $width;
	private $height;
	
	private $absoluteX;
	private $absoluteY;
	
	public function __construct(
		string $bodyPart, 
		string $whiteImage,
		string $blackImage, 
		int $width, 
		int $height, 
		int $xOffset = NULL, 
		int $yOffset = NULL, 
		int $parentAnchorX = NULL, 
		int $parentAnchorY = NULL
	)
	{
		$this->children = [];
		$this->bodyPart = $bodyPart;
		$this->whiteImage = $whiteImage;
		$this->blackImage = $blackImage;
		$this->xOffset = $xOffset;
		$this->yOffset = $yOffset;
		$this->parentAnchorX = $parentAnchorX;
		$this->parentAnchorY = $parentAnchorY;
		$this->width = $width;
		$this->height = $height;
	}

    /**
     * @codeCoverageIgnore
     */
	public function parentAnchorX() : int
	{
		return $this->parentAnchorX;
	}

    /**
     * @codeCoverageIgnore
     */
	public function parentAnchorY() : int
	{
		return $this->parentAnchorY;
	}

    /**
     * @codeCoverageIgnore
     */
	public function addChild(BodyPart $bodyPart) : void
	{
		$this->children []= $bodyPart;
	}

    /**
     * Once all the body parts have been added to the tree, this function will trigger walking the tree
     * and calculating their positions within the composited image.
     */
	public function imageInfo() : array
	{
		$minsAndMaxes = $this->calculateAbsoluteCoordinates();
		
		$xOffset = $minsAndMaxes["xMin"] < 0 ? $minsAndMaxes["xMin"] * -1 : 0;
		$yOffset = $minsAndMaxes["yMin"] < 0 ? $minsAndMaxes["yMin"] * -1 : 0;
		$width = $xOffset + $minsAndMaxes["xMax"];
		$height = $yOffset + $minsAndMaxes["yMax"];
		
		$imageCoordinates = $this->shiftCoordinates($xOffset, $yOffset);
		
		return ["width" => $width, "height" => $height, "images" => $imageCoordinates];
	}
	
	/**
	 *  Pushes over all the images by an x and y offset, to ensure the final image is not off center,
	 *  and returns the final set of coordinates as array.
	 */
	public function shiftCoordinates(int $xOffset, int $yOffset) : array
	{
		$this->absoluteX += $xOffset;
		$this->absoluteY += $yOffset;
		
		$images = [
			$this->bodyPart => [
			    "whiteImage" => $this->whiteImage,
                "blackImage" => $this->blackImage,
                "xOffset" => $this->absoluteX,
                "yOffset" => $this->absoluteY,
            ],
		];
		
		foreach ($this->children as $child) {
			$images = array_merge($images, $child->shiftCoordinates($xOffset, $yOffset));
		}
		
		return $images;
	}
	
	/**
	 *  We have to walk the "tree" of images to figure out their "coordinates" for placement in the final image.
	 *  There is also the problem that depending on the configuration of the body part images, the resulting image may end
	 *  up off center, and so this function also returns an array that can be used to calculate if everything needs to be pushed
	 *  over, and the size required by the composited image.
	 */
	public function calculateAbsoluteCoordinates(int $xOffset = 0, int $yOffset = 0) : array
	{
		$absoluteX = $xOffset - $this->xOffset;
		$absoluteY = $yOffset - $this->yOffset;
		$absoluteXMax = $absoluteX + $this->width;
		$absoluteYMax = $absoluteY + $this->height;
		$absoluteXMin = $absoluteX;
		$absoluteYMin = $absoluteY;
		
		$this->absoluteX = $absoluteX;
		$this->absoluteY = $absoluteY;
		
		foreach ($this->children as $child)
		{
			$minsAndMaxes = $child->calculateAbsoluteCoordinates($absoluteX + $child->parentAnchorX(), 
				$absoluteY + $child->parentAnchorY());
				
			$absoluteXMax = max($absoluteXMax, $minsAndMaxes["xMax"]);
			$absoluteYMax = max($absoluteYMax, $minsAndMaxes["yMax"]);
			$absoluteXMin = min($absoluteXMin, $minsAndMaxes["xMin"]);
			$absoluteYMin = min($absoluteYMin, $minsAndMaxes["yMin"]);
		}
		
		return ["xMax" => $absoluteXMax, "yMax" => $absoluteYMax, "xMin" => $absoluteXMin, "yMin" => $absoluteYMin];
	}
}