<?php declare(strict_types=1);

require dirname(__DIR__).'/vendor/autoload.php';

define("MAX_IMAGE_SIZE", 500);
define("MIN_IMAGE_SIZE", 100);
define("BODY_PART_IMAGES_LOCATION", "images/");
define("IMAGE_PADDING", 5);
define("MAX_SIZE_RANGE", 255);
define("BODY_PART_IMAGES_INFO_XML_FILE", dirname(__DIR__) . "/bodyPartsConfig.xml");