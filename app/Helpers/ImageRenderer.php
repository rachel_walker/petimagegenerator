<?php declare(strict_types=1);

/**
 * @category     Helper
 * @package      petImageGenerator
 * @author       Rachel Walker (rachel.kai@gmail.com)
 */

namespace App\Helpers;

use Exception;
use Imagick;
use ImagickPixel;

class ImageRenderer 
{
    public function __construct()
    {

    }

	public function new(int $width, int $height) : Imagick
    {
        $image = new Imagick();
        $image->newImage($width, $height, new ImagickPixel("none"));
        $image->setImageFormat("png");

        return $image;
    }
	
	/*
	 *  Ensures values denoting color are in proper hex format.
	 */ 
	private function normalizeHex(string $string) : string
	{
		//Is it hex?
		if (ctype_xdigit($string))
		{
			//If the hex string is too short, add padding zeroes.
			if (strlen($string) < 6)
			{
				str_pad($string, 6 - strlen($string), "0", STR_PAD_LEFT);
			}
			//If the hex string is too long, chop off the extra digits.
			elseif (strlen($string) > 6)
			{
				$string = substr($string, 0, 6);
			}
			return $string;
		}
		
		//string is not a hex value, use defaults instead.
		return "ffffff";
	}
	
	public function imageDimensions(string $fileName) : array
	{
		$imagePath = BODY_PART_IMAGES_LOCATION . $fileName;
		if (!file_exists($imagePath)) {
			throw new Exception("Body part image {$fileName} does not exist.");
		}
		
		$image = new Imagick($imagePath);
		
		return [
			$image->getImageWidth(),
			$image->getImageHeight(),
		];
	}
	
	public function resizeImage(Imagick $image, int $size, int $finalImageHeight, int $finalImageWidth, string $backgroundColor) : Imagick
	{
		$backgroundColor = $this->normalizeHex($backgroundColor);
		
		$finalImageHeight = $finalImageHeight <= MAX_IMAGE_SIZE ? $finalImageHeight : MAX_IMAGE_SIZE;
		$finalImageWidth = $finalImageWidth <= MAX_IMAGE_SIZE ? $finalImageWidth : MAX_IMAGE_SIZE;
		
		$finalImageHeight = $finalImageHeight >= MIN_IMAGE_SIZE ? $finalImageHeight : MIN_IMAGE_SIZE;
		$finalImageWidth = $finalImageWidth >= MIN_IMAGE_SIZE ? $finalImageWidth : MIN_IMAGE_SIZE;
		
		$imageHeightWithPadding = $finalImageHeight - (2 * IMAGE_PADDING);
		$imageWidthWithPadding = $finalImageWidth - (2 * IMAGE_PADDING);
		
		$width = $image->getImageWidth();
		$height = $image->getImageHeight();
		
		$normalization = $width - $imageWidthWithPadding >= $height - $imageHeightWithPadding ? 
			$imageWidthWithPadding / $width : $imageHeightWithPadding / $height;
		$normalization = $normalization <= 1 ? $normalization : 1;

		$size = $size >= MAX_SIZE_RANGE ? MAX_SIZE_RANGE : $size;
		$size = $size <= 0 ? 0 : $size;

		$percentage = ($size / MAX_SIZE_RANGE) * .5 + .5;
		$newWidth = (int) floor($width * $normalization * $percentage);
		$newHeight = (int) floor($height * $normalization * $percentage);

		$image->scaleImage($newWidth,$newHeight);
		
		$background = new Imagick();
		$background->newImage($finalImageWidth, $finalImageHeight, new ImagickPixel("#" . $backgroundColor));
		$background->setImageFormat('png');
		$xOffset = (int) floor(($finalImageWidth - $newWidth) / 2);
		$yOffset = (int) $finalImageHeight - $newHeight - IMAGE_PADDING;
		$background->compositeImage($image, Imagick::COMPOSITE_DEFAULT, $xOffset, $yOffset);
		$background->flattenImages();

		return $background;
	}
	
	private function colorizePart(string $color, string $whiteImage, string $blackImage) : Imagick
	{
        list($width, $height) = $this->imageDimensions($whiteImage);

		$whitePath = BODY_PART_IMAGES_LOCATION . $whiteImage;
		$blackPath = BODY_PART_IMAGES_LOCATION . $blackImage;
		$whiteImage = new Imagick($whitePath);
		$blackImage = new Imagick($blackPath);
		
		$image = new Imagick();
		$image->newImage($width, $height, new ImagickPixel("#" . $color));
		$image->setImageFormat('png');
		$image->compositeImage($whiteImage, imagick::COMPOSITE_MULTIPLY, 0, 0);
		$image->compositeImage($blackImage, imagick::COMPOSITE_SCREEN, 0, 0);
		$image->compositeImage($whiteImage, imagick::COMPOSITE_COPYOPACITY, 0, 0);
		$image->flattenImages();

		return $image;
	}
	
	public function addBodyPart(Imagick $image, string $color, string $whiteImage, string $blackImage, int $xOffset, int $yOffset) : Imagick
	{
		$color = $this->normalizeHex($color);
		
		$part = $this->colorizePart($color, $whiteImage, $blackImage);
		$image->compositeImage($part, Imagick::COMPOSITE_DEFAULT, $xOffset, $yOffset);
		$image->flattenImages();

		return $image;
	}
}