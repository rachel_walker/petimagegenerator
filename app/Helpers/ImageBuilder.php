<?php declare(strict_types=1);

/**
 * This class is the glue between the xml data, read by BodyPartsConfig class, a tree representation of the body parts
 * in order to figure out the exact placement of each body part in the overall image, and the class that actually
 * manipulates the image data.
 *
 * @category     Helper
 * @package      petImageGenerator
 * @author       Rachel Walker (rachel.kai@gmail.com)
 */

namespace App\Helpers;

use App\Models\BodyPartsConfig;
use App\Models\BodyPart;
use Imagick;

class ImageBuilder
{
	private $size;
	private $imageHeight;
	private $imageWidth;
	private $tailNumber;
	private $neckNumber;
	private $headNumber;
	private $bodyNumber;
	private $legNumber = 0;
	private $eyeNumber = 0;
	private $bodyColor;
	private $eyeColor;
	private $backgroundColor;

	/** @var BodyPartsConfig */
	private $bodyPartsConfig;

	/** @var ImageRenderer */
	private $imageRenderer;

    /**
     * Body parts must have the proper layer order and use the correct color property
     * when being assembled into the final image.
     */
	const BODY_PART_LAYER_ORDER_AND_COLOR = [
        BodyPartsConfig::BODY_PART_TAIL => "bodyColor",
        BodyPartsConfig::BODY_PART_RIGHT_HIND_LEG => "bodyColor",
        BodyPartsConfig::BODY_PART_RIGHT_FRONT_LEG => "bodyColor",
        BodyPartsConfig::BODY_PART_BODY => "bodyColor",
        BodyPartsConfig::BODY_PART_NECK => "bodyColor",
        BodyPartsConfig::BODY_PART_LEFT_HIND_LEG => "bodyColor",
        BodyPartsConfig::BODY_PART_LEFT_FRONT_LEG => "bodyColor",
        BodyPartsConfig::BODY_PART_HEAD => "bodyColor",
        BodyPartsConfig::BODY_PART_EYE => "eyeColor",
    ];
	
	public function __construct(
		BodyPartsConfig $bodyPartsConfig,
        ImageRenderer $imageRenderer,
		int $size, 
		int $imageHeight, 
		int $imageWidth, 
		int $tailNumber,
		int $neckNumber,
		int $headNumber,
		int $bodyNumber,
		string $bodyColor, 
		string $eyeColor, 
		string $backgroundColor
	)
	{
		$this->bodyPartsConfig = $bodyPartsConfig;

		$this->imageRenderer = $imageRenderer;

		$this->size = $size;
		$this->imageHeight = $imageHeight;
		$this->imageWidth = $imageWidth;
		$this->tailNumber = $tailNumber;
		$this->neckNumber = $neckNumber;
		$this->headNumber = $headNumber;
		$this->bodyNumber = $bodyNumber;
		$this->bodyColor = $bodyColor;
		$this->eyeColor = $eyeColor;
		$this->backgroundColor = $backgroundColor;
	}

	/**
     * Helper function creates a body part node based on the given information.
     */
	private function createBodyPart(
		string $bodyPart, 
		int $bodyPartNumber, 
		string $anchorPointName = NULL, 
		string $parentBodyPart = NULL, 
		int $parentBodyPartNumber = NULL,
		string $parentAnchorPointName = NULL
	) : BodyPart
	{
		if (!is_null($anchorPointName)) {
			list($xOffset, $yOffset) = $this->bodyPartsConfig->getAnchorOffset($bodyPart, $bodyPartNumber, $anchorPointName);
		}
		else {
			$xOffset = 0;
			$yOffset = 0;
		}
		
		if (!is_null($parentAnchorPointName) && !is_null($parentAnchorPointName) && !is_null($parentBodyPartNumber)) {
			list($parentXOffset, $parentYOffset) = $this->bodyPartsConfig->getAnchorOffset($parentBodyPart, $parentBodyPartNumber, $parentAnchorPointName);
		}
		else {
			$parentXOffset = 0;
			$parentYOffset = 0;
		}
		
		$whiteImage = $this->bodyPartsConfig->getImage($bodyPart, $bodyPartNumber, "white");
		$blackImage = $this->bodyPartsConfig->getImage($bodyPart, $bodyPartNumber, "black");
		
		list($width, $height) = $this->imageRenderer->imageDimensions($whiteImage);
		
		return new BodyPart($bodyPart, $whiteImage, $blackImage, $width, $height, $xOffset, $yOffset, $parentXOffset, $parentYOffset);
	}

	/**
     * CalculateImagePositions assembles the body parts in a tree structure, so that the locations of each part image within the composite image
     * can be calculated.
     */
	private function calculateImagePositions() : array
    {
        //Using the head as the root of the tree, therefore it does not attach to anything, so NULL is passed.
        $head = $this->createBodyPart(
            BodyPartsConfig::BODY_PART_HEAD,
            $this->headNumber,
            NULL,
            NULL,
            NULL,
            NULL
        );

        $eye = $this->createBodyPart(
            BodyPartsConfig::BODY_PART_EYE,
            $this->eyeNumber,
            BodyPartsConfig::ANCHOR_TYPE_EYE_HEAD,
            BodyPartsConfig::BODY_PART_HEAD,
            $this->headNumber,
            BodyPartsConfig::ANCHOR_TYPE_HEAD_EYE
        );
        $head->addChild($eye);

        $neck = $this->createBodyPart(
            BodyPartsConfig::BODY_PART_NECK,
            $this->neckNumber,
            BodyPartsConfig::ANCHOR_TYPE_NECK_HEAD,
            BodyPartsConfig::BODY_PART_HEAD,
            $this->headNumber,
            BodyPartsConfig::ANCHOR_TYPE_HEAD_NECK
        );
        $head->addChild($neck);

        $body = $this->createBodyPart(
            BodyPartsConfig::BODY_PART_BODY,
            $this->bodyNumber,
            BodyPartsConfig::ANCHOR_TYPE_BODY_NECK,
            BodyPartsConfig::BODY_PART_NECK,
            $this->neckNumber,
            BodyPartsConfig::ANCHOR_TYPE_NECK_BODY
        );
        $neck->addChild($body);

        //The legs break the anchor point pattern a bit, I did not make separate anchor points on the body for them,
        //instead reusing the anchor points for the neck and tail.
        $rightFrontLeg = $this->createBodyPart(
            BodyPartsConfig::BODY_PART_RIGHT_FRONT_LEG,
            $this->legNumber,
            BodyPartsConfig::ANCHOR_TYPE_RIGHT_FRONT_LEG_BODY,
            BodyPartsConfig::BODY_PART_BODY,
            $this->bodyNumber,
            BodyPartsConfig::ANCHOR_TYPE_BODY_NECK
        );
        $body->addChild($rightFrontLeg);

        $leftFrontLeg = $this->createBodyPart(
            BodyPartsConfig::BODY_PART_LEFT_FRONT_LEG,
            $this->legNumber,
            BodyPartsConfig::ANCHOR_TYPE_LEFT_FRONT_LEG_BODY,
            BodyPartsConfig::BODY_PART_BODY,
            $this->bodyNumber,
            BodyPartsConfig::ANCHOR_TYPE_BODY_NECK
        );
        $body->addChild($leftFrontLeg);

        $rightHindLeg = $this->createBodyPart(
            BodyPartsConfig::BODY_PART_RIGHT_HIND_LEG,
            $this->legNumber,
            BodyPartsConfig::ANCHOR_TYPE_RIGHT_HIND_LEG_BODY,
            BodyPartsConfig::BODY_PART_BODY,
            $this->bodyNumber,
            BodyPartsConfig::ANCHOR_TYPE_BODY_TAIL
        );
        $body->addChild($rightHindLeg);

        $leftHindLeg = $this->createBodyPart(
            BodyPartsConfig::BODY_PART_LEFT_HIND_LEG,
            $this->legNumber,
            BodyPartsConfig::ANCHOR_TYPE_LEFT_HIND_LEG_BODY,
            BodyPartsConfig::BODY_PART_BODY,
            $this->bodyNumber,
            BodyPartsConfig::ANCHOR_TYPE_BODY_TAIL
        );
        $body->addChild($leftHindLeg);

        $tail = $this->createBodyPart(
            BodyPartsConfig::BODY_PART_TAIL,
            $this->tailNumber,
            BodyPartsConfig::ANCHOR_TYPE_TAIL_BODY,
            BodyPartsConfig::BODY_PART_BODY,
            $this->bodyNumber,
            BodyPartsConfig::ANCHOR_TYPE_BODY_TAIL
        );
        $body->addChild($tail);

        return $head->imageInfo();
    }

    /**
     * drawImage actually draws the image once we have all the necessary data.
     */
    private function drawImage(array $imageInfo) : Imagick
    {
        $image = $this->imageRenderer->new($imageInfo["width"], $imageInfo["height"]);
        $bodyPartsImageInfo = $imageInfo["images"];

        foreach (self::BODY_PART_LAYER_ORDER_AND_COLOR as $bodyPart => $color) {
            $image = $this->imageRenderer->addBodyPart(
                $image,
                $this->{$color},
                $bodyPartsImageInfo[$bodyPart]["whiteImage"],
                $bodyPartsImageInfo[$bodyPart]["blackImage"],
                $bodyPartsImageInfo[$bodyPart]["xOffset"],
                $bodyPartsImageInfo[$bodyPart]["yOffset"]
            );
        }
		
		$image = $this->imageRenderer->resizeImage($image, $this->size, $this->imageHeight, $this->imageWidth, $this->backgroundColor);

        return $image;
    }
	
	public function build() : Imagick
	{		
		$imageInfo = $this->calculateImagePositions();
		return $this->drawImage($imageInfo);
	}
}