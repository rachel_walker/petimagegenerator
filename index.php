<?php declare(strict_types=1);

use App\Helpers\ImageBuilder;
use App\Helpers\ImageRenderer;
use App\Models\BodyPartsConfig;

require __DIR__.'/app/bootstrap.php';

header('Content-type: image/png');

$size = isset($_GET["size"]) ? (int)$_GET["size"] : MAX_SIZE_RANGE;
$imageHeight = isset($_GET["imageHeight"]) ? (int)$_GET["imageHeight"] : MAX_IMAGE_SIZE;
$imageWidth =  isset($_GET["imageWidth"]) ? (int)$_GET["imageWidth"] : MAX_IMAGE_SIZE;
$tailNumber = (int)$_GET["tailNumber"];
$neckNumber = (int)$_GET["neckNumber"];
$headNumber = (int)$_GET["headNumber"];
$bodyNumber = (int)$_GET["bodyNumber"];
$bodyColor = (string)$_GET["bodyColor"];
$eyeColor = (string)$_GET["eyeColor"];
$backgroundColor = (string)$_GET["backgroundColor"];

try {
    $imageRenderer = new ImageRenderer();
    $bodyPartsConfig = new BodyPartsConfig(BODY_PART_IMAGES_INFO_XML_FILE);

    $imageBuilder = new ImageBuilder(
        $bodyPartsConfig,
        $imageRenderer,
        $size,
        $imageHeight,
        $imageWidth,
        $tailNumber,
        $neckNumber,
        $headNumber,
        $bodyNumber,
        $bodyColor,
        $eyeColor,
        $backgroundColor
    );

    echo $imageBuilder->build();
}
catch (Throwable $e) {
    error_log($e->getTraceAsString());
}