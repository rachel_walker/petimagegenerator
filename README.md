# Pet Image Generator

Composites a full pet image from images of bodyparts, each part is also dynamically colorized.

An xml file contains data for the anchor points of each body part.  The body parts are designed so that they can be swapped in and out with each other without obvious seams, as long as they connect to the correct point.

Regarding anchor points, each part has an anchor point which should line up exactly with the corresponding anchor point of the body part it's attaching to.  For example, there is an anchor point on the head, which needs to line up with an anchor point on the neck.

Regarding colorizing the pet, each body part has a black version and a white version.  These two images are composited over the desired color using the screen and multiply filters, resulting in a final image.  By doing it this way, it makes shading and certain features (like claws) consistent coloring, rather than also color shifted.

### Requirements

* PHP ~7.3 
* Imagick php extension
* Composer
* Xdebug (if running unit tests)

### Installation

* `composer install`
* `phpunit -c phpunit.xml tests/unit` (for running unit tests)

### Usage

See a live example at http://cryogenia.com/~meruru/petProject/index.php

Certain parameters can be adjusted by adding to a query string:

* `size:` 0 through 255.  Adjusts how big the pet appears in the overall image.
* `imageHeight:` 100 to 500.  Adjusts the height of the generated image in pixels.
* `imageWidth:` 100 to 500.  Adjusts the width of the generated image in pixels.
* `tailNumber:` 0 to 6.  Swaps in various tail images.
* `neckNumber:` 0 to 4.  Swaps in various neck images.
* `bodyNumber:` 0 to 5.  Swaps in various body images.
* `headNumber:` 0 to 5.  Swaps in various head images.
* `backgroundColor:` hex color of the format ff00ff.
* `bodyColor:`  hex color of the format ff00ff.
* `eyeColor:` hex color of the format ff00ff.

If any invalid values are given, it will be forced to be a valid value.