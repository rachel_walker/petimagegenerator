<?php declare(strict_types=1);

/**
 * @category     Unit Test
 * @package      petImageGenerator
 * @author       Rachel Walker (rachel.kai@gmail.com)
 */

namespace Unit\Models;

use App\Models\BodyPartsConfig;
use Exception;
use PHPUnit\Framework\TestCase;

class BodyPartsConfigTest extends TestCase
{
    private $fixturesDir = null;

    protected function setUp() : void
    {
        $this->fixturesDir = dirname(dirname(__DIR__)) . "/fixtures/";
    }

    public function testConstructBadFilePath() : void
    {
		$this->expectException(Exception::class);
		$this->expectExceptionMessage("The body part config file does not exist.");
	
        $bodyPartsConfig = new BodyPartsConfig($this->fixturesDir . "test.xml");
    }

    public function testGetAnchorInvalidBodyPart() : void
    {
		$this->expectException(Exception::class);
		$this->expectExceptionMessage("Invalid body part");
		
        $bodyPartsConfig = new BodyPartsConfig($this->fixturesDir . "getAnchorTest.xml");
        $bodyPartsConfig->getAnchorOffset("bodyPart", 1, "anchorType");
    }

    public function testGetAnchorInvalidBodyPartNumber() : void
    {
        $bodyPartsConfig = new BodyPartsConfig($this->fixturesDir . "getAnchorTest.xml");
        list($xOffset, $yOffset) = $bodyPartsConfig->getAnchorOffset("tail", 2, "tailBody");

        $this->assertEquals(6, $xOffset);
        $this->assertEquals(1, $yOffset);
    }

    public function testGetAnchorTypeDoesNotExist() : void
    {
		$this->expectException(Exception::class);
		$this->expectExceptionMessage("Anchor type anchorType not found for tail #0");
		
        $bodyPartsConfig = new BodyPartsConfig($this->fixturesDir . "getAnchorTest.xml");
        $anchor = $bodyPartsConfig->getAnchorOffset("tail", 0, "anchorType");
    }

    public function testGetAnchorSuccess() : void
    {
        $bodyPartsConfig = new BodyPartsConfig($this->fixturesDir . "getAnchorTest.xml");
        list($xOffset, $yOffset) = $bodyPartsConfig->getAnchorOffset("tail", 1, "tailBody");

        $this->assertEquals(5, $xOffset);
        $this->assertEquals(5, $yOffset);
    }

    public function testGetImageInvalidBodyPart() : void
    {
		$this->expectException(Exception::class);
		$this->expectExceptionMessage("Invalid body part");
		
        $bodyPartsConfig = new BodyPartsConfig($this->fixturesDir . "getImageTest.xml");
        $bodyPartsConfig->getImage("bodyPart", 1, "white");
    }

    public function testGetImageInvalidImageType() : void
    {
		$this->expectException(Exception::class);
		$this->expectExceptionMessage("Image can only be black or white");
		
        $bodyPartsConfig = new BodyPartsConfig($this->fixturesDir . "getImageTest.xml");
        $bodyPartsConfig->getImage("body", 1, "type");
    }

    public function testGetImageInvalidBodyPartNumber() : void
    {
        $bodyPartsConfig = new BodyPartsConfig($this->fixturesDir . "getImageTest.xml");
        $image = $bodyPartsConfig->getImage("tail", 2, "white");

        $this->assertEquals("tails/tail0_white.png", $image);
    }

    public function testGetImageSuccess() : void
    {
        $bodyPartsConfig = new BodyPartsConfig($this->fixturesDir . "getImageTest.xml");
        $image = $bodyPartsConfig->getImage("tail", 1, "black");

        $this->assertEquals("tails/tail1_black.png", $image);
    }
}