<?php declare(strict_types=1);
/**
 * @category     Unit Test
 * @package      petImageGenerator
 * @copyright    Copyright (c) 2019 Julep Beauty, Inc. (www.julep.com)
 * @author       Rachel Walker (rachel.kai@gmail.com)
 */

namespace Unit\Models;

use App\Models\BodyPart;
use PHPUnit\Framework\TestCase;

class BodyPartTest extends TestCase
{
    public function testCalculateAbsoluteCoordinatesSetOne() : void
    {
        $root = new BodyPart(
            "root",
            "root_white.png",
            "root_black.png",
            10,
            20,
            null,
            null,
            null,
            null
        );

        $firstChild = new BodyPart(
            "firstChild",
            "first_child_white.png",
            "first_child_black.png",
            10,
            10,
            0,
            0,
            10,
            10
        );

        $root->addChild($firstChild);

        $secondChild = new BodyPart(
            "secondChild",
            "second_child_white.png",
            "second_child_black.png",
            10,
            10,
            10,
            10,
            0,
            0
        );
        $root->addChild($secondChild);

        $thirdChild = new BodyPart(
            "thirdChild",
            "third_child_white.png",
            "third_child_black.png",
            10,
            10,
            0,
            0,
            10,
            10
        );
        $firstChild->addChild($thirdChild);

        $minsAndMaxes = $root->calculateAbsoluteCoordinates();

        $this->assertEquals([
            "xMax" => 30,
            "yMax" => 30,
            "xMin" => -10,
            "yMin" => - 10,
        ], $minsAndMaxes);

        $imageInfo = $root->shiftCoordinates(10, 10);
        $this->assertEquals([
            "root" => [
                "whiteImage" => "root_white.png",
                "blackImage" => "root_black.png",
                "xOffset" => 10,
                "yOffset" => 10,
            ],
            "firstChild" => [
                "whiteImage" => "first_child_white.png",
                "blackImage" => "first_child_black.png",
                "xOffset" => 20,
                "yOffset" => 20,
            ],
            "secondChild" => [
                "whiteImage" => "second_child_white.png",
                "blackImage" => "second_child_black.png",
                "xOffset" => 0,
                "yOffset" => 0,
            ],
            "thirdChild" => [
                "whiteImage" => "third_child_white.png",
                "blackImage" => "third_child_black.png",
                "xOffset" => 30,
                "yOffset" => 30,
            ],
        ], $imageInfo);
    }

    public function testCalculateAbsoluteCoordinatesSetTwo() : void
    {
        $root = new BodyPart(
            "root",
            "root_white.png",
            "root_black.png",
            10,
            20,
            null,
            null,
            null,
            null
        );

        $firstChild = new BodyPart(
            "firstChild",
            "first_child_white.png",
            "first_child_black.png",
            20,
            10,
            0,
            0,
            10,
            20
        );
        $root->addChild($firstChild);

        $secondChild = new BodyPart(
            "secondChild",
            "second_child_white.png",
            "second_child_black.png",
            10,
            10,
            0,
            0,
            0,
            0
        );
        $root->addChild($secondChild);

        $thirdChild = new BodyPart(
            "thirdChild",
            "third_child_white.png",
            "third_child_black.png",
            10,
            10,
            0,
            0,
            10,
            10
        );
        $firstChild->addChild($thirdChild);

        $minsAndMaxes = $root->calculateAbsoluteCoordinates();

        $this->assertEquals([
            "xMax" => 30,
            "yMax" => 40,
            "xMin" => 0,
            "yMin" => 0,
        ], $minsAndMaxes);

        $imageInfo = $root->shiftCoordinates(0, 0);
        $this->assertEquals([
            "root" => [
                "whiteImage" => "root_white.png",
                "blackImage" => "root_black.png",
                "xOffset" => 0,
                "yOffset" => 0,
            ],
            "firstChild" => [
                "whiteImage" => "first_child_white.png",
                "blackImage" => "first_child_black.png",
                "xOffset" => 10,
                "yOffset" => 20,
            ],
            "secondChild" => [
                "whiteImage" => "second_child_white.png",
                "blackImage" => "second_child_black.png",
                "xOffset" => 0,
                "yOffset" => 0,
            ],
            "thirdChild" => [
                "whiteImage" => "third_child_white.png",
                "blackImage" => "third_child_black.png",
                "xOffset" => 20,
                "yOffset" => 30,
            ],
        ], $imageInfo);
    }

    public function testImageInfo() : void
    {
        $root = new BodyPart(
            "root",
            "root_white.png",
            "root_black.png",
            10,
            20,
            null,
            null,
            null,
            null
        );

        $firstChild = new BodyPart(
            "firstChild",
            "first_child_white.png",
            "first_child_black.png",
            20,
            10,
            5,
            5,
            10,
            5
        );
        $root->addChild($firstChild);

        $secondChild = new BodyPart(
            "secondChild",
            "second_child_white.png",
            "second_child_black.png",
            10,
            10,
            5,
            5,
            0,
            0
        );
        $firstChild->addChild($secondChild);

        $thirdChild = new BodyPart(
            "thirdChild",
            "third_child_white.png",
            "third_child_black.png",
            100,
            100,
            100,
            100,
            0,
            0
        );
        $secondChild->addChild($thirdChild);

        $results = $root->imageInfo();

        $this->assertEquals([
            "width" => 125,
            "height" => 125,
            "images" => [
                "root" => [
                    "whiteImage" => "root_white.png",
                    "blackImage" => "root_black.png",
                    "xOffset" => 100,
                    "yOffset" => 105,
                ],
                "firstChild" => [
                    "whiteImage" => "first_child_white.png",
                    "blackImage" => "first_child_black.png",
                    "xOffset" => 105,
                    "yOffset" => 105,
                ],
                "secondChild" => [
                    "whiteImage" => "second_child_white.png",
                    "blackImage" => "second_child_black.png",
                    "xOffset" => 100,
                    "yOffset" => 100,
                ],
                "thirdChild" => [
                    "whiteImage" => "third_child_white.png",
                    "blackImage" => "third_child_black.png",
                    "xOffset" => 0,
                    "yOffset" => 0,
                ],
            ],
        ], $results);
    }
}